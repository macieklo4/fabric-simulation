Simulation of net-like fabric made in Unity using "Verlet integration"<br />

WHILE SIMULATNG:<br />
-**left click** on a point to drag it on the plane (either locked or unlocked)<br />

WHILE NOT SIMULATING:<br />
-**right click** on empty space to create point<br />
-**left click** on a point to connect it with a previous point with a stick<br />
-press **escape** to delete previous anchor point (then left click on point to select new anchor)

BOTH:<br />
-press **c** to clear the scene<br />
-press **space** to start/stop the simulation<br />
-hold **right click** and drag the mouse on sticks to cut them<br />
-**right click** on a point to lock/unlock it<br />

![image](/uploads/9193917987959fe67fbb1cb1872916c2/image.png)
